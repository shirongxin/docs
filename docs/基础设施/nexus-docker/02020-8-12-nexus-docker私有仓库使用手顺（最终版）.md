---
layout: post
title: 【nexus】私有仓库使用手顺（最终版）！！---- [总结：](#总结)
- [一，作为docker私有仓库使用](#一作为docker私有仓库使用)
  - [1，客户端配置](#1客户端配置)
  - [2. 客户端使用](#2-客户端使用)
    - [2.1 必须登陆之后才能下载。](#21-必须登陆之后才能下载)
    - [2.2 拉取镜像](#22-拉取镜像)
    - [2.3 push镜像到私有仓库](#23-push镜像到私有仓库)
- [二，作为maven仓库使用](#二作为maven仓库使用)
  - [1. 新建一个maven工程](#1-新建一个maven工程)
  - [2. pom.xml 指定deploy的位置](#2-pomxml-指定deploy的位置)
  - [3. setting.xml](#3-settingxml)
  - [4.验证下载](#4验证下载)
- [三，使用私有npm仓库](#三使用私有npm仓库)
  - [1. 创建一个测试项目](#1-创建一个测试项目)
  - [2. 配置](#2-配置)
    - [把用户信息写入配置文件（就无需npm login）](#把用户信息写入配置文件就无需npm-login)
        - [Linux：](#linux)
        - [windows:](#windows)
      - [写入“.npmrc”文件](#写入npmrc文件)
  - [3. 下载依赖的使用方法](#3-下载依赖的使用方法)
  - [4. 发布包的方法](#4-发布包的方法)

## 总结：
| Nexus现状  |  docker | npm  |  maven |
|---|---|---| ---|
| http  | 1.1 pull=OK / push OK  | 1.2 fetch=OK / publish=OK  | 1.3 pull = OK / deploy=OK |
| https  | 2.1 OK   | 2.2 NG(弃用)  | 2.3 NG(弃用) |

| 库 |  url |使用方法|
|---|---|---|
|nexus web 管理：| http://nexus.ccbjb.com.cn | |
|maven-public：|http://nexus.ccbjb.com.cn/repository/maven-public/ |用来下载依赖 |
|maven-releases：|http://nexus.ccbjb.com.cn/repository/maven-releases/ |用来发布 |
|maven-snapshots：|http://nexus.ccbjb.com.cn/repository/maven-snapshots/ |用来发布 |
|npm-group|http://nexus.ccbjb.com.cn/repository/npm-group/|用来下载依赖|
|npm-internal|http://nexus.ccbjb.com.cn/repository/npm-internal/|用来发布|
|npm-group|http://nexus.ccbjb.com.cn/repository/npm-group/|用来下载依赖|
|npm-proxy|http://nexus.ccbjb.com.cn/repository/npm-proxy/|用来下载依赖|
|docker-public|docker.ccbjb.com.cn|用来下载镜像，通过docker cli的method重定向到别的端口|
|docker-hub|docker.ccbjb.com.cn|用来下载镜像，通过docker cli的method重定向到别的端口|
|docker-private|docker.ccbjb.com.cn|用来上传镜像，通过docker cli的method重定向到别的端口|
---
## 一，作为docker私有仓库使用
拉取尽量使用本地仓库，这样会加快拉取速度。
镜像不存在时，会自动下载镜像。
一般外网下载1分钟，本地仓库只需要15秒。

### 1，客户端配置
 1.1 无需修改 /etc/docker/daemon.json
 1.2 创建根证书保存目录
   - linux：`mkdir -p /etc/docker/certs.d/docker.ccbjb.com.cn`
   - windows：创建目录`C:\Program Files\Docker\Docker\certs.d\docker.ccbjb.com.cn`
     
 1.3 下载根证书复制到docker根证书目录
   -  下载common工程`git@gitlab.ccbjb.com.cn:ec7mongrp/common.git` 
   -  linux : 拷贝到`common/docker.ccbjb.com.cnRoot证书/root.crt` 到`/etc/docker/certs.d/docker.ccbjb.com.cn/`
   -  windows：拷贝到`C:\Program Files\Docker\Docker\certs.d\docker.ccbjb.com.cn\root.crt`
 1.4 检查DNS设置为`192.168.99.2` ,否则无法识别域名`docker.ccbjb.com.cn`

### 2. 客户端使用
#### 2.1 必须登陆之后才能下载。
私有docker仓库地址是docker.ccbjb.com.cn
用户名每个人都不同。初始密码123.com，必须登陆到nexus.ccbjb.com.cn上修改新密码。
```bash
root@centos125:~# docker login -u mengxt docker.ccbjb.com.cn
Password:
WARNING! Your password will be stored unencrypted in /root/.docker/config.json.
Configure a credential helper to remove this warning. See
https://docs.docker.com/engine/reference/commandline/login/#credentials-store
Login Succeeded
```
#### 2.2 拉取镜像
如果不确定镜像名，可以到dockerhub.com上搜索，搜到后右上角会有pull命令。
镜像名前加上docker.ccbjb.com.cn这个域名

```bash
root@centos125:~# docker pull docker.ccbjb.com.cn/redis:latest
latest: Pulling from redis
bf5952930446: Pull complete
911b8422b695: Pull complete
093b947e0ade: Pull complete
5b1d5f59e382: Pull complete
7a5f59580c0b: Pull complete
f9c63997c980: Pull complete
Digest: sha256:09c33840ec47815dc0351f1eca3befe741d7105b3e95bc8fdb9a7e4985b9e1e5
Status: Downloaded newer image for docker.ccbjb.com.cn/redis:latest
docker.ccbjb.com.cn/redis:latest
root@centos125:~# docker images
REPOSITORY                  TAG                 IMAGE ID            CREATED             SIZE
docker.ccbjb.com.cn/redis   latest              1319b1eaa0b7        7 days ago          104MB
alpine                      latest              a24bb4013296        2 months ago        5.57MB
root@centos125:~#
```
#### 2.3 push镜像到私有仓库
```bash
root@centos125:~# docker tag alpine:latest docker.ccbjb.com.cn/alpine:latest
root@centos125:~# docker push docker.ccbjb.com.cn/alpine:latest
```
http://nexus.ccbjb.com.cn上可以查看该镜像
![](/docs/images/2020-08-12-16-10-34.png)
---
## 二，作为maven仓库使用
### 1. 新建一个maven工程
https://maven.apache.org/guides/getting-started/index.html#How_do_I_make_my_first_Maven_project

例如F:\work\maven test\my-app
`mvn -B archetype:generate -DgroupId=com.mycompany.app -DartifactId=my-app -DarchetypeArtifactId=maven-archetype-quickstart -DarchetypeVersion=1.4`



### 2. pom.xml 指定deploy的位置
```
<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>cn.com.ccbjb.app</groupId>
  <artifactId>srx-app</artifactId>
  <version>1.0-RELEASE</version>
  <!-- <version>1.0-SNAPSHOT</version> -->

  <name>my-app</name>
  <!-- FIXME change it to the project's website -->
  <url>http://www.example.com</url>

  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <maven.compiler.source>1.8</maven.compiler.source>
    <maven.compiler.target>1.8</maven.compiler.target>
  </properties>

  <dependencies>
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>4.11</version>
      <scope>test</scope>
    </dependency>
    <!-- <dependency>
      <groupId>struts</groupId>
      <artifactId>struts</artifactId>
      <version>1.2.9</version>
    </dependency> -->
    <dependency>
      <groupId>org.hibernate</groupId>
      <artifactId>hibernate</artifactId>
      <version>3.6.0.Beta2</version>
      <type>pom</type>
    </dependency>
  </dependencies>

  <build>
    <pluginManagement>      <!-- lock down plugins versions to avoid using Maven defaults (may be moved to parent pom) -->
      <plugins>
        <!-- clean lifecycle, see https://maven.apache.org/ref/current/maven-core/lifecycles.html#clean_Lifecycle -->
        <plugin>
          <artifactId>maven-clean-plugin</artifactId>
          <version>3.1.0</version>
        </plugin>
        <!-- default lifecycle, jar packaging: see https://maven.apache.org/ref/current/maven-core/default-bindings.html#Plugin_bindings_for_jar_packaging -->
        <plugin>
          <artifactId>maven-resources-plugin</artifactId>
          <version>3.0.2</version>
        </plugin>
        <plugin>
          <artifactId>maven-compiler-plugin</artifactId>
          <version>3.8.0</version>
        </plugin>
        <plugin>
          <artifactId>maven-surefire-plugin</artifactId>
          <version>2.22.1</version>
        </plugin>
        <plugin>
          <artifactId>maven-jar-plugin</artifactId>
          <version>3.0.2</version>
        </plugin>
        <plugin>
          <artifactId>maven-install-plugin</artifactId>
          <version>2.5.2</version>
        </plugin>
        <plugin>
          <artifactId>maven-deploy-plugin</artifactId>
          <version>2.8.2</version>
        </plugin>
        <!-- site lifecycle, see https://maven.apache.org/ref/current/maven-core/lifecycles.html#site_Lifecycle -->
        <plugin>
          <artifactId>maven-site-plugin</artifactId>
          <version>3.7.1</version>
        </plugin>
        <plugin>
          <artifactId>maven-project-info-reports-plugin</artifactId>
          <version>3.0.0</version>
        </plugin>
      </plugins>
    </pluginManagement>
  </build>

  <distributionManagement>
    <repository>
      <id>nexus-release</id>
      <name>Nexus Release Repository</name>
      <url>http://nexus.ccbjb.com.cn/repository/maven-releases/</url>
    </repository>
    <snapshotRepository>
      <id>nexus-snapshot</id>
      <name>Nexus Snapshot Repository</name>
      <url>http://nexus.ccbjb.com.cn/repository/maven-snapshots/</url>
    </snapshotRepository>
  </distributionManagement>
</project>

```
根据artifact的版本名字来自动决定发送到release库还是snapshot库。


### 3. setting.xml
C:\Program Files (x86)\apache-maven-3.6.3\conf\settings.xml

自己的用户名：例如mengxt，和公司邮件@前面的ID一样
自己的密码：初始密码123.com，请到http://nexus.ccbjb.com.cn上修改

```
## 维护服务器的验证信息。ID是唯一标识
      <server>
        <id>nexus</id>
        <username>自己的用户名</username>
        <password>自己的密码</password>
      </server>
      <server>
        <id>nexus-release</id>
        <username>自己的用户名</username>
        <password>自己的密码</password>
      </server>
      <server>
        <id>nexus-snapshot</id>
        <username>自己的用户名</username>
        <password>自己的密码</password>
      </server>
    </servers>
...

## 会覆盖所有下载库的url最后都会转到这个地址来下载
     <mirror>
        <id>nexus</id>
        <mirrorOf>*</mirrorOf>
        <name>Nexus myself</name>
        <url>http://nexus.ccbjb.com.cn/repository/maven-public/</url>
      </mirror>  
...

## 依赖库和插件库指定。
<profile>
        <id>mycof</id>
        <repositories>
          <!-- 私有库地址-->
          <repository>
            <id>nexus</id>
            <url>http://nexus.ccbjb.com.cn/repository/maven-public/</url>
            <releases>
              <enabled>true</enabled>
            </releases>
            <snapshots>
              <enabled>true</enabled>
            </snapshots>
          </repository>
        </repositories>
        <pluginRepositories>
          <!--插件库地址-->
          <pluginRepository>
            <id>nexus</id>
            <url>http://nexus.ccbjb.com.cn/repository/maven-public/</url>
            <releases>
              <enabled>true</enabled>
            </releases>
            <snapshots>
              <enabled>true</enabled>
            </snapshots>
          </pluginRepository>
        </pluginRepositories>
  </profile>

   <!--激活profile-->
  <activeProfiles>
      <activeProfile>mycof</activeProfile>
    </activeProfiles>
  </settings>
```

### 4.验证下载
`mvn clean && mvn compile`
会自动下载依赖，到私服上。然后到本地。

`mvn deploy`
会发布这个包到私服-private库。
可以到http://nexus.ccbjb.com.cn上查看maven库已经包含此构件。
---## 三，使用私有npm仓库
入口：https://guides.sonatype.com/repo3/quick-start-guides/proxying-maven-and-npm/
### 1. 创建一个测试项目
```
mkdir z-tools
cd z-tools
npm init
```
// vi index.js
```
!function(){
  console.log(`这是引入的包入口`)
}()
```
### 2. 配置
// 修改package.json 增加一行，这是指明发布的地址。
// 注意发布到**npm内部库！**
```
"publishConfig": {
    "registry": "http://nexus.ccbjb.com.cn/repository/npm-internal/"
  },
```

#### 把用户信息写入配置文件（就无需npm login）
原本npm install 和 npm publish都需要用户权限。
把用户名密码写到配置文件中，免得每次都输入用户名密码。
###### Linux：
`echo -n 'admin:admin123' | openssl base64`
（**这里输入你自己的用户名和密码**）
得到结果
```bash
-----BEGIN CERTIFICATE-----
YWRtaW46YWRtaW4xMjM=
-----END CERTIFICATE----- 
```

###### windows:
1. 创建文件in.txt，内容如下
 `admin:admin123`  
2. 运行`c:\certutil /encode in.txt out.txt`
out.txt
得到结果
```bash
-----BEGIN CERTIFICATE-----
YWRtaW46YWRtaW4xMjM=
-----END CERTIFICATE----- 
```


##### 写入“.npmrc”文件

- linux:vim /root/.npmrc
- windows:C:\Users\用户名\.npmrc

// 这里指明拉取的库是group库，这是最佳实践。
```bash
registry=http://nexus.ccbjb.com.cn/repository/npm-group/
//nexus.ccbjb.com.cn/repository/:_authToken=NpmToken.0f6396e0-605e-3820-8656-2f62e1c4526b
//nexus.ccbjb.com.cn/repository/npm-group/:_authToken=NpmToken.0f6396e0-605e-3820-8656-2f62e1c4526b
init.author.name=shirx // 写你自己的用户名
init.author.email=shirx@ccbjb.com.cn //自己的信息
init.author.url=https://shirongxin.gitlab.io/docs //自己的信息
email=shirx@ccbjb.com.cn //自己的信息
always-auth=true
_auth="YWRtaW46YWRtaW4xMjM="
```

::: danger 注意！
 registry 后的url必须以/结束，否则会截掉最后的npm-internal
 最佳实践：
  1. “.npmrc”中设置拉取库。
    `registry=http://nexus.ccbjb.com.cn/repository/npm-group/`
  2. “package.json"中设置发布库。
  ```json
  "publishConfig" : {
      "registry" : "http://nexus.ccbjb.com.cn/repository/npm-internal/"
  },
  ```
:::

### 3. 下载依赖的使用方法
::: warning 注意
.npmrc中一定设置成npm-group库（group库）否则不能下载依赖！
:::
//不确定的话可以执行get看看
`npm config get registry`
```bash
[root@centos122 npm]# npm install vue
npm notice created a lockfile as package-lock.json. You should commit this file. + vue@2.6.11
added 1 package from 1 contributor in 15.962s
```
![](/docs/images/2020-08-06-13-19-13.png)


### 4. 发布包的方法
::: warning 注意
package.json中一定设置成npm-internal库（内部库）否则发布不成功！
:::
package.json
```json
"publishConfig" : {
    "registry" : "http://nexus.ccbjb.com.cn/repository/npm-internal/"
},
```

```bash
[root@centos122 z-tool]# npm config get registry
http://nexus.ccbjb.com.cn/repository/npm-internal/

[root@centos122 z-tool]# npm publish
npm notice
npm notice �  srxztool@1.0.0
npm notice === Tarball Contents ===
npm notice 393B package.json.bak
npm notice 59B  index.js
npm notice 298B package.json
npm notice === Tarball Details ===
npm notice name:          srxztool
npm notice version:       1.0.0
npm notice package size:  480 B
npm notice unpacked size: 750 B
npm notice shasum:        bc5dc7fb2596a6dae2320d9b84f005da898e5b9d
npm notice integrity:     sha512-OHlaBEIsh5o7U[...]gLLQ0iR9xSV4Q==
npm notice total files:   3
npm notice
+ srxztool@1.0.0
[root@centos122 z-tool]#
```

完了。